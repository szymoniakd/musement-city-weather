<?php

namespace App\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\NativeHttpClient;

class WeatherApiTest extends TestCase
{
    public function testCoordinatesWeather()
    {
        $client = new NativeHttpClient();
        $response = $client->request('GET', 'http://api.weatherapi.com/v1/forecast.json', [
            'query' => [
                'key' => $_ENV['WEATHER_API_KEY'],
                'q' => '39.739,-104.993',
                'days' => 2,
            ]
        ]);

        $this->assertEquals(200, $response->getStatusCode());

        $data = json_decode($response->getContent(), true);

        $this->assertArrayHasKey('forecast', $data);

        $forecast = $data['forecast'];
        $this->assertArrayHasKey('forecastday', $forecast);

        $forecastDay = $forecast['forecastday'];
        $this->assertIsArray($forecastDay);

        $today = $forecastDay[0];
        $this->assertArrayHasKey('day', $today);

        $day = $today['day'];
        $this->assertArrayHasKey('condition', $day);

        $condition = $day['condition'];
        $this->assertArrayHasKey('text', $condition);

        $text = $condition['text'];
        $this->assertIsString($text);
    }
}