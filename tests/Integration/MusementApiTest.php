<?php

namespace App\Tests\Integration;

use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\NativeHttpClient;

class MusementApiTest extends TestCase
{
    public function testCityList()
    {
        $client = new NativeHttpClient();
        $response = $client->request('GET', 'https://api.musement.com/api/v3/cities');

        $this->assertEquals(200, $response->getStatusCode());

        $content = $response->getContent();
        $data = json_decode($content, true);

        $this->assertGreaterThan(0, count($data));

        $city = $data[0];
        $this->assertArrayHasKey('name', $city);
        $this->assertArrayHasKey('latitude', $city);
        $this->assertArrayHasKey('longitude', $city);

        $this->assertNotEmpty($city['name']);
        $this->assertIsFloat($city['latitude']);
        $this->assertIsFloat($city['longitude']);
    }
}