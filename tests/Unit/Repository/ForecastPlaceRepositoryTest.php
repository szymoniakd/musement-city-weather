<?php

namespace App\Tests\Unit\Repository;

use App\Dto\Coordinates;
use App\Entity\City;
use App\Entity\Forecast;
use App\Entity\ForecastPlace;
use App\Exception\PlaceForecastFetchError;
use App\Repository\ForecastPlaceRepository;
use App\Service\Contract\CityProviderInterface;
use App\Service\Contract\WeatherServiceProviderInterface;
use PHPUnit\Framework\TestCase;
use Symfony\Component\HttpClient\Exception\TransportException;

class ForecastPlaceRepositoryTest extends TestCase
{
    public function testList()
    {
        $repository = new ForecastPlaceRepository($this->getCityProviderMock(), $this->getWeatherProviderMock());

        $collection = $repository->list();
        $firstElement = $collection[0];
        $this->assertInstanceOf(ForecastPlace::class, $firstElement);
    }

    public function testListOnWeatherProviderError()
    {
        $repository = new ForecastPlaceRepository($this->getCityProviderMock(), $this->getWeatherProviderFaultyMock());

        $collection = $repository->list();
        $firstElement = $collection[0];
        $this->assertInstanceOf(ForecastPlace::class, $firstElement);
    }

    public function testListOnCityProviderError()
    {
        $repository = new ForecastPlaceRepository($this->getCityProviderFaultyMock(), $this->getWeatherProviderMock());

        $collection = $repository->list();
        $this->assertCount(0, $collection);
    }

    private function getCityProviderMock()
    {
        $mock = $this->createMock(CityProviderInterface::class);
        $mock
            ->method('list')
            ->will($this->returnValue([
                new City('Denver', new Coordinates(39.739, -104.993))
            ]));

        return $mock;
    }

    private function getCityProviderFaultyMock()
    {
        $mock = $this->createMock(CityProviderInterface::class);
        $mock
            ->method('list')
            ->will($this->returnValue([]));

        return $mock;
    }

    private function getWeatherProviderMock()
    {
        $mock = $this->createMock(WeatherServiceProviderInterface::class);
        $mock
            ->method('getWeather')
            ->will($this->returnValue(new Forecast('Today forecast', 'Tomorrow forecast')));

        return $mock;
    }

    private function getWeatherProviderFaultyMock()
    {
        $mock = $this->createMock(WeatherServiceProviderInterface::class);
        $mock
            ->method('getWeather')
            ->will($this->throwException(new PlaceForecastFetchError()));

        return $mock;
    }
}