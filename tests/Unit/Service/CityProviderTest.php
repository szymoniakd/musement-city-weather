<?php

namespace App\Tests\Unit\Service;

use App\Dto\Coordinates;
use App\Entity\City;

class CityProviderTest extends AbstractServiceTest
{
    public function testList()
    {
        $this->setHttpMockJsonResponse($this->getTestResponseJson());
        $cityProvider = $this->getCityProvider();

        $result = $cityProvider->list();
        $this->assertIsArray($result);

        $city = $result[0];

        $this->assertInstanceOf(City::class, $city);
        $this->assertEquals(new City("Denver", new Coordinates(39.739, -104.993)), $city);
    }

    public function testShouldReturnEmptyResultOnError()
    {
        $this->setHttpMockResponse($this->getThrowTransportExceptionResponse());

        $cityProvider = $this->getCityProvider();
        $result = $cityProvider->list();
        $this->assertEquals([], $result);
    }

    private function getTestResponseJson()
    {
        return '[{"name":"Denver","latitude":39.739,"longitude":-104.993}]';
    }
}
