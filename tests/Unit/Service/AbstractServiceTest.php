<?php

namespace App\Tests\Unit\Service;

use App\Factory\CityFactory;
use App\Factory\Contract\CityFactoryInterface;
use App\Service\CityProvider;
use App\Service\WeatherServiceProvider;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Symfony\Component\HttpClient\Exception\TransportException;
use Symfony\Component\HttpClient\MockHttpClient;
use Symfony\Component\HttpClient\Response\MockResponse;
use Symfony\Contracts\HttpClient\HttpClientInterface;

abstract class AbstractServiceTest extends TestCase
{
    private $httpMockResponse = null;

    public function setHttpMockResponse(callable $httpMockResponse): void
    {
        $this->httpMockResponse = $httpMockResponse;
    }

    protected function getWeatherService()
    {
        return new WeatherServiceProvider($this->getHttpClientMock($this->httpMockResponse), new NullLogger());
    }

    private function getHttpClientMock(?callable $callback)
    {
        return new MockHttpClient($callback);
    }

    protected function setHttpMockJsonResponse(string $json)
    {
        $this->setHttpMockResponse(function () use ($json) {
            return new MockResponse($json);
        });
    }

    protected function getThrowTransportExceptionResponse()
    {
        return function () {
            throw new TransportException();
        };
    }

    protected function getCityFactory()
    {
        return new CityFactory();
    }

    protected function getCityProvider(
        HttpClientInterface $httpClientMock = null,
        CityFactoryInterface $cityFactory = null,
        LoggerInterface $logger = null
    )
    {
        if (!$httpClientMock) {
            $httpClientMock = $this->getHttpClientMock($this->httpMockResponse);
        }

        if (!$cityFactory) {
            $cityFactory = $this->getCityFactory();
        }

        if (!$logger) {
            $logger = new NullLogger();
        }

        return new CityProvider($httpClientMock, $cityFactory, $logger);
    }
}