<?php

namespace App\Tests\Unit\Entity;

use App\Dto\Coordinates;
use App\Entity\City;
use App\Entity\Forecast;
use App\Entity\ForecastPlace;
use PHPUnit\Framework\TestCase;

class ForecastPlaceTest extends TestCase
{
    public function testDescribe()
    {
        $forecastPlace = new ForecastPlace(
            new City('Denver', new Coordinates(1.0, 1.0)),
            new Forecast('Today forecast', 'Tomorrow forecast')
        );

        $this->assertEquals('Denver | Today forecast - Tomorrow forecast', $forecastPlace->describe());
    }
}