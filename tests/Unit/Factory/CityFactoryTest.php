<?php

namespace App\Tests\Unit\Factory;

use App\Exception\IncompleteDataException;
use App\Factory\CityFactory;
use PHPUnit\Framework\TestCase;

class CityFactoryTest extends TestCase
{
    public function testFactory()
    {
        $factory = new CityFactory();

        $city = $factory->factory([
            'name' => 'Denver',
            'latitude' => 39.739,
            'longitude' => -104.993,
        ]);
        $this->assertEquals('Denver', $city->getName());
        $this->assertEquals(39.739, $city->getCoordinates()->getLatitude());
        $this->assertEquals(-104.993, $city->getCoordinates()->getLongitude());

        $this->expectException(IncompleteDataException::class);
        $factory->factory([]);
    }
}