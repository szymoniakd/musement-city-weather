<?php

namespace App\Entity;

use App\Entity\Contract\ForecastDescribableInterface;
use App\Entity\Contract\PlaceInterface;

class ForecastPlace
{
    private $place;

    private $forecast;

    public function __construct(PlaceInterface $place, ForecastDescribableInterface $forecast)
    {
        $this->place = $place;
        $this->forecast = $forecast;
    }

    public function describe(): string
    {
        return sprintf(
            '%s | %s - %s',
            $this->place->getName(),
            $this->forecast->getTodayForecast(),
            $this->forecast->getTomorrowForecast()
        );
    }
}