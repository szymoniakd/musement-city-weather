<?php

namespace App\Entity\Contract;

use App\Dto\Coordinates;

interface WeatherCheckInterface
{
    public function getCoordinates(): Coordinates;
}