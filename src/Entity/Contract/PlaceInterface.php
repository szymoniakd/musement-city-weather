<?php

namespace App\Entity\Contract;

interface PlaceInterface
{
    public function getName(): string;
}