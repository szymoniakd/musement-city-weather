<?php

namespace App\Entity\Contract;

interface ForecastDescribableInterface
{
    public function getTodayForecast(): string;

    public function getTomorrowForecast(): string;
}