<?php

namespace App\Entity\Contract;

interface ForecastPlaceInterface
{
    public function describe(): string;
}