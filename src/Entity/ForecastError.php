<?php

namespace App\Entity;

use App\Entity\Contract\ForecastDescribableInterface;

class ForecastError implements ForecastDescribableInterface
{
    public function getTodayForecast(): string
    {
        return 'No forecast available now';
    }

    public function getTomorrowForecast(): string
    {
        return 'No forecast available now';
    }
}