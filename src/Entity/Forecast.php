<?php

namespace App\Entity;

use App\Entity\Contract\ForecastDescribableInterface;

class Forecast implements ForecastDescribableInterface
{
    private $todayForecast;

    private $tomorrowForecast;

    public function __construct(string $todayForecast, string $tomorrowForecast)
    {
        $this->todayForecast = $todayForecast;
        $this->tomorrowForecast = $tomorrowForecast;
    }

    public function getTodayForecast(): string
    {
        return $this->todayForecast;
    }

    public function getTomorrowForecast(): string
    {
        return $this->tomorrowForecast;
    }
}