<?php

namespace App\Entity;

use App\Dto\Coordinates;
use App\Entity\Contract\PlaceInterface;
use App\Entity\Contract\WeatherCheckInterface;

class City implements WeatherCheckInterface, PlaceInterface
{
    private $name;

    private $coordinates;

    public function __construct(string $name, Coordinates $coordinates)
    {
        $this->name = $name;
        $this->coordinates = $coordinates;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getCoordinates(): Coordinates
    {
        return $this->coordinates;
    }
}