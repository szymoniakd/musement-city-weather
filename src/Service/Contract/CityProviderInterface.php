<?php

namespace App\Service\Contract;

use App\Entity\City;

interface CityProviderInterface
{
    /**
     * @return City[]
     */
    public function list(): array;
}