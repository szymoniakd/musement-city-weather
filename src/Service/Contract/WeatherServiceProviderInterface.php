<?php

namespace App\Service\Contract;

use App\Dto\Coordinates;
use App\Entity\Forecast;
use App\Exception\PlaceForecastFetchError;

interface WeatherServiceProviderInterface
{
    /**
     * @param Coordinates $coordinates
     * @return Forecast
     * @throws PlaceForecastFetchError
     */
    public function getWeather(Coordinates $coordinates);
}