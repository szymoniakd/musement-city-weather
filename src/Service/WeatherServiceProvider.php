<?php

namespace App\Service;

use App\Dto\Coordinates;
use App\Entity\Forecast;
use App\Exception\PlaceForecastFetchError;
use App\Service\Contract\WeatherServiceProviderInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WeatherServiceProvider implements WeatherServiceProviderInterface
{
    private $client;

    private $logger;

    private $url = 'http://api.weatherapi.com/v1/forecast.json';

    public function __construct(HttpClientInterface $client, LoggerInterface $logger)
    {
        $this->client = $client;
        $this->logger = $logger;
    }

    public function getWeather(Coordinates $coordinates): Forecast
    {
        try {
            $response = $this->client->request('GET', $this->url, [
                'query' => $this->getRequestQuery($coordinates)
            ])->getContent();
        } catch (\Throwable $e) { // the easy way for simplicity of this solution
            $this->logger->critical('Problem while fetching forecast: ' . $e->getMessage());
            throw new PlaceForecastFetchError();
        }
        return $this->parseResponseToForecast($this->decode($response));
    }

    private function parseResponseToForecast(array $data): Forecast
    {
        $today = $data['forecast']['forecastday'][0];
        $tomorrow = $data['forecast']['forecastday'][1];
        $weatherToday = $today['day']['condition']['text'];
        $weatherTomorrow = $tomorrow['day']['condition']['text'];
        return new Forecast($weatherToday, $weatherTomorrow);
    }

    private function coordinatesToQuery(Coordinates $coordinates): string
    {
        return sprintf('%f,%f', $coordinates->getLatitude(), $coordinates->getLongitude());
    }

    private function getRequestQuery(Coordinates $coordinates): array
    {
        return [
            'key' => $_ENV['WEATHER_API_KEY'],
            'q' => $this->coordinatesToQuery($coordinates),
            'days' => 2,
        ];
    }

    private function decode(string $response): array
    {
        return json_decode($response, true);
    }
}