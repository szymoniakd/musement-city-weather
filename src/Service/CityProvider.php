<?php

namespace App\Service;

use App\Exception\IncompleteDataException;
use App\Factory\Contract\CityFactoryInterface;
use App\Service\Contract\CityProviderInterface;
use Psr\Log\LoggerInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class CityProvider implements CityProviderInterface
{
    private $httpClient;

    private $cityFactory;

    private $logger;

    private $url = 'https://api.musement.com/api/v3/cities';

    public function __construct(HttpClientInterface $client, CityFactoryInterface $cityFactory, LoggerInterface $logger)
    {
        $this->httpClient = $client;
        $this->cityFactory = $cityFactory;
        $this->logger = $logger;
    }

    /**
     * @inheritDoc
     */
    public function list(): array
    {
        try {
            $response = $this->httpClient->request('GET', $this->url);
            $cities = json_decode($response->getContent(), true);
        } catch (\Throwable $e) { // the easy way for simplicity of this solution
            $this->logger->critical('Problem while fetching cities: ' . $e->getMessage());
            return [];
        }

        $collection = [];
        foreach ($cities as $city) {
            try {
                $collection[] = $this->cityFactory->factory($city);
            } catch (IncompleteDataException $e) {
                $this->logger->critical('Problem while factoring city: ' . $e->getMessage());
            }
        }

        return $collection;
    }
}