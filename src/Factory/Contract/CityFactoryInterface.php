<?php

namespace App\Factory\Contract;

use App\Entity\City;
use App\Exception\IncompleteDataException;

interface CityFactoryInterface
{
    /**
     * @param array $data
     * @return City
     * @throws IncompleteDataException
     */
    public function factory(array $data): City;
}