<?php

namespace App\Factory;

use App\Dto\Coordinates;
use App\Entity\City;
use App\Exception\IncompleteDataException;
use App\Factory\Contract\CityFactoryInterface;

class CityFactory implements CityFactoryInterface
{
    public function factory(array $data): City
    {
        if (!isset($data['name']) || !isset($data['latitude']) || !isset($data['longitude'])) {
            throw new IncompleteDataException('City should have name, latitude and longitude');
        }
        return new City($data['name'], new Coordinates($data['latitude'], $data['longitude']));
    }
}