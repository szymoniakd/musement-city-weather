<?php

namespace App\Command;

use App\Repository\Contract\ForecastPlaceRepositoryInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class GetWeather extends Command
{
    private $forecastPlaceRepository;

    protected static $defaultName = 'weather:get';

    public function __construct(
        ForecastPlaceRepositoryInterface $forecastPlaceRepository
    )
    {
        $this->forecastPlaceRepository = $forecastPlaceRepository;
        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        foreach ($this->forecastPlaceRepository->list() as $item) {
            $output->writeln($item->describe());
        }

        return static::SUCCESS;
    }
}