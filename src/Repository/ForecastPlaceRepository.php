<?php

namespace App\Repository;

use App\Entity\ForecastError;
use App\Entity\ForecastPlace;
use App\Exception\PlaceForecastFetchError;
use App\Repository\Contract\ForecastPlaceRepositoryInterface;
use App\Service\Contract\CityProviderInterface;
use App\Service\Contract\WeatherServiceProviderInterface;

class ForecastPlaceRepository implements ForecastPlaceRepositoryInterface
{
    private $cityProvider;

    private $weatherServiceProvider;

    public function __construct(
        CityProviderInterface $cityProvider,
        WeatherServiceProviderInterface $weatherServiceProvider
    )
    {
        $this->cityProvider = $cityProvider;
        $this->weatherServiceProvider = $weatherServiceProvider;
    }

    /**
     * @inheritdoc
     */
    public function list(): array
    {
        $cities = $this->cityProvider->list();

        $collection = [];
        foreach ($cities as $city) {
            try {
                $forecast = $this->weatherServiceProvider->getWeather($city->getCoordinates());
            } catch (PlaceForecastFetchError $e) {
                $forecast = new ForecastError();
            }
            $collection[] = new ForecastPlace($city, $forecast);
        }

        return $collection;
    }
}