<?php

namespace App\Repository\Contract;

use App\Entity\Contract\ForecastPlaceInterface;

interface ForecastPlaceRepositoryInterface
{
    /**
     * @return ForecastPlaceInterface[]
     */
    public function list(): array;
}