# Get Cities Forecast App

## Installation

First of all copy `.env.example` to `.env` and provide `WEATHER_API_KEY`.
Then just run: 

```
composer install
```

To perform forecast check run:

```
php bin/console weather:get
```

## Tests

```
./vendor/bin/phpunit
```